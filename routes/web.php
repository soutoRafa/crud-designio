<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'userController@ReadUser');
Route::get('/adicionar-novo', 'userController@newUser');
Route::post('/cadastrar', 'userController@CreateUser');
Route::get('/editar-usuario/{id}', 'userController@userInformation');
Route::post('/editar/{id}/', 'userController@updateUser');
Route::post('/excluir-usuario/{id}/', 'userController@deleteUser');