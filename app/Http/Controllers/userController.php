<?php

namespace crud\Http\Controllers;

use Request;
use crud\User;
use crud\Phone;
use session;
use crud\Http\Requests\CreateRequest;
use crud\Http\Requests\UpdateRequest;
use Illuminate\Support\facades\DB;
use Carbon\Carbon;
class userController extends Controller
{
   public function newUser()
   {
    // retorna view do formulário de cadastro

       return view('cadastro');
   }

    public function CreateUser(CreateRequest $request)
    {
    	$user = new User;
    	$user->name = Request::input('nome');
    	$user->email = Request::input('email');
    	$user->address = Request::input('endereco');
    	$user->cep = Request::input('cep');
    	$user->city = Request::input('cidade');
    	$user->state = Request::input('estado');
    	//conversão para data padrão do datetime
    	$data = strtr(Request::input('dtnasc'), '/', '-');

        $user->birthday = Carbon::parse($data)->format('Y-m-d');
        
    	$numbers = Request::input('fone');
    	
        $user->save();
    	
        // Insert dinâmico para N número de telefones
        
        if($numbers != null){
           
            foreach ($numbers as $numb) {   
                $phone = new Phone;
                $phone->number = $numb;
                $phone->user_id = $user->id;
                
                if($phone->number != NULL){
                   $user->telefones()->save($phone);
                }
            
            }   
        }
        
    	return redirect('/');
    } 

    public function ReadUser()
    {
        
    	$users = User::with('telefones')->orderBy('name')->get();

    	return view('usuarios')->with('users', $users);
    }

    public function userInformation($id)
    {
    	$user = User::with('telefones')->find($id);
        

    	return view('edicao')->with('user', $user);
    }

    public function updateUser(UpdateRequest $request, $id)
    {
       
    	$user = User::with('telefones')->find($id);

        $user->name = Request::input('nome');
       	$user->email = Request::input('email');
       	$user->address = Request::input('endereco');
       	$user->cep = Request::input('cep');
       	$user->city = Request::input('cidade');
       	$user->state = Request::input('estado');
       
        //conversão para data padrão do datetime	
        $data = strtr(Request::input('dtnasc'), '/', '-');
       	$user->birthday = Carbon::parse($data)->format('Y-m-d');
       	$numbers = Request::input('fone');
        $user->save();
        
        // Update dinâmico para 1 ou mais telefones
        
        $phone = Phone::with('usuario')->where('user_id', $id);
        $flag = 0;
        foreach ($user->telefones as $u) {
            $u->number = $numbers[$flag];
            $u->user_id = $id;

           $user->telefones()->save($u);
            $flag++;
        }
        
    	return back();
    }

    public function deleteUser($id)
    {
    	User::destroy($id);

		return redirect('/');
    }
}
