<?php

namespace crud\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
      public function rules()
    {
        $hoje = Carbon::now()->format('d-m-Y');
       
        return [
            'nome' => 'required',
            'email' => 'required|unique:users,id,'.$this->id,
            'dtnasc' => 'required|min:8|date_format:d/m/Y|before:'.$hoje,
            'endereco' => 'required',
            'cep' => 'required|min:5',
            'cidade' => 'required',
            'estado' => 'required',
        ];
    }
    public function messages()
    {
        return[
            'nome.required' => 'O campo nome é obrigatório',
            'email.required' => 'O campo email é obrigatório',
            'dtnasc.required' => 'O campo data de nascimento é obrigatório',
            'endereco.required' => 'O campo endereço é obrigatório',
            'cep.required' => 'O campo cep é obrigatório',
            'cidade.required' => 'O campo cidade é obrigatório',
            'estado.required' => 'O campo estado é obrigatório',
            'dtnasc.min' => 'O campo data de nascimento deve ter pelo menos 8 caracteres',
            'cep.min' => 'O campo cep deve ter pelo menos 5 caracteres',
            'email.unique' => 'O campo email deve ser único',
            'dtnasc.date_format' => 'O campo data de nascimento deve ser formatado: dia, mês e ano',
            'dtnasc.before' => 'O campo data de nascimento precisa ser de uma data anterior a aual',
        ];
    }
}
