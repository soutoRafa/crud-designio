<?php

namespace crud;

use Illuminate\Database\Eloquent\Model;
use crud\User;
use Illuminate\Database\Eloquent\Relations\belongsTo;

class Phone extends Model
{
     protected $fillable = [
        'number', 'user_id',
    ];


    public function usuario()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
}
