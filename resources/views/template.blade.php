<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<link rel="stylesheet" type="text/css" href="/css/estilos.css">
</head>
<body>
	<div class="navbar navbar-static-top navbar-default">
		<div class="container">
			<div class="navbar-brand">
				CRUD Laravel 5.4
			</div>
			
		</div>
	</div>
	
		@yield('content')
	<script type="text/javascript" src="/js/app.js"></script>
	<script type="text/javascript" src="/js/jquery.mask.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.date').mask('00/00/0000');
			$('.fone-ddd').mask('(00) 00000-0000');

			$('.add-fone').click( function(){
				$('.novo-campo').append('<div class="form-group">'+
											'<label>Telefone</label>'+
											'<input type="text" name="fone[]" class="form-control fone-ddd" id="campoDinamico">'+
						    			'</div>');
				$('.fone-ddd').mask('(00) 00000-0000');
			});			
		});
	</script>
	
</body>
</html>