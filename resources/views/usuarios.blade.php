@extends('template')
@section('title')

Lista de usuarios

@stop

@section('content')
	<h1>Lista de usuários</h1>

	<form action="/adicionar-novo" method="get">
		<button class="btn btn-primary">Adicionar novo usuário</button>
	</form>

	<table class="table">
		<thead>
			<tr>
				
				<th>Editar</th>
				<th>Excluir</th>
				<th>Nome</th>
				<th>Email</th>
				<th>Dt Nascimento</th>
				<th>CEP</th>
				<th>Endereço</th>
				<th>Cidade</th>
				<th>Estado</th>
				<th>Telefones</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $u)
			
			<tr>
				
				
				<td>
					<form action="/editar-usuario/{{$u->id}}" method="get">
						
						<button class="btn btn-primary" value="{{ $u->id }}">Editar</button>
					</form>
				</td>
				
				<td>
					<form action="/excluir-usuario/{{$u->id}}" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<button class="btn btn-danger" type="submit">Excluir</button>
					</form>
				</td>

				<td> {{$u->name}} </td>
				<td> {{$u->email}} </td>
				<td> {{ date("d/m/Y", strtotime($u->birthday)) }} </td>
				<td> {{$u->cep}} </td>
				<td> {{$u->address}} </td>
				<td> {{$u->city}} </td>
				<td> {{$u->state}} </td>
				<td>
					@foreach($u->telefones as $t)
						
						 {{ $t->number }} <br>
						
					@endforeach
				</td>
			</tr>

			@endforeach
		</tbody>
	</table>
@stop