@extends('template')
@section('title')

Página inicial

@stop

<div class="container">
	@section('content')
		<div class="col-md-12">
			<h1>Cadastrar usuário</h1>
			@if( count( $errors->all() ) > 0 )
				<div class="alert alert-danger">
					@foreach($errors->all() as $error)
						<p>{{ $error }} </p>
					@endforeach	
				</div>
			@endif
			<form action="/cadastrar" method="post" id="form">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label for="nome"> Nome </label>
					<input type="text" name="nome" id="nome" class="form-control" value="{{ old('nome') }}">
				</div>
				<div class="form-group">
					<label for="email"> Email </label>
					<input type="email" name="email" id="email" class="form-control" value="{{old('email')}}">
				</div>
				<div class="form-group">
					<label for="nascimento"> Data de nascimento </label>
					<input type="text" name="dtnasc" id="nascimento" class="form-control date" value="{{old('dtnasc')}}">
				</div>
				<div class="form-group">
					<label for="endereco"> Endereço </label>
					<input type="text" name="endereco" id="endereco" class="form-control" value="{{old('endereco')}}">
				</div>
				<div class="form-group">
					<label for="cep"> CEP </label>
					<input type="text" name="cep" id="cep" class="form-control" value="{{old('cep')}}">
				</div>
				<div class="form-group">
					<label for="cidade"> Cidade </label>
					<input type="text" name="cidade" id="cidade" class="form-control" value="{{old('cidade')}}">
				</div>
				<div class="form-group">
					<label for="estado"> Estado </label>
					<input type="text" name="estado" id="estado" class="form-control"  value="{{old('estado')}}">
				</div>
				<div class="form-group fone">
					<label for="fone"> Telefone </label>
					<input type="text" name="fone[]" id="fone" class="form-control fone-ddd">
				</div>
				<div class="novo-campo">
					
				</div>
				<button class="btn btn-link add-fone" type="button">Adicionar mais um telefone?</button>
				
				<br>

				<button class="btn btn-primary" type="submit"> Cadastrar </button>
			</form>
		</div>	

	@stop
</div>