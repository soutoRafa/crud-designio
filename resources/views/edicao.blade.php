@extends('template')
@section('title')

Formulário de edição

@stop


	@section('content')
	<div class="container">
		<a href="/">Voltar a listagem de usuários!</span></a>
		<div class="col-md-12">
			<h1>Editar usuário</h1>
			<div class="col-md-12">
			
			@if( count( $errors->all() ) > 0 )
				<div class="alert alert-danger">
					@foreach($errors->all() as $error)
						<p>{{ $error }} </p>
					@endforeach	
				</div>
			@endif
		
			<form action="/editar/{{$user->id}}" method="post">

			<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label for="nome"> Nome </label>
					<input type="text" name="nome" id="nome" class="form-control" value="{{ $user->name }}">
				</div>
				<div class="form-group">
					<label for="email"> Email </label>
					<input type="email" name="email" id="email" class="form-control" value="{{ $user->email }}">
				</div>
				<div class="form-group">
					<label for="nascimento"> Data de nascimento </label>
					<input type="text" name="dtnasc" id="nascimento" class="form-control date" value="{{($user->birthday->format('d/m/Y')) }}">
				</div>
				<div class="form-group">
					<label for="endereco"> Endereço </label>
					<input type="text" name="endereco" id="endereco" class="form-control" value="{{ $user->address }}">
				</div>
				<div class="form-group">
					<label for="cep"> CEP </label>
					<input type="text" name="cep" id="cep" class="form-control" value="{{ $user->cep }}">
				</div>
				<div class="form-group">
					<label for="cidade"> Cidade </label>
					<input type="text" name="cidade" id="cidade" class="form-control" value="{{ $user->city }}">
				</div>
				<div class="form-group">
					<label for="estado"> Estado </label>
					<input type="text" name="estado" id="estado" class="form-control" value="{{ $user->state }}">
				</div>

				@foreach($user->telefones as $t)
				
					<div class="form-group fone">
						<label for="fone"> Telefone </label>
						<input type="text" name="fone[]" id="fone" class="form-control fone-ddd" value="{{ $t->number }}">
					</div>
					
				@endforeach
				
				
				<br>
				<button class="btn btn-primary" type="submit"> Editar </button>
			</form>
			
		</div>	
</div>
	@stop
