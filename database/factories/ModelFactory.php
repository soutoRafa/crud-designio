<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(crud\User::class, function (Faker\Generator $faker) {
   
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'birthday' => $faker->datetime(),
        'city' => $faker->city,
        'state' => $faker->state,
        'address' => $faker->streetAddress,
        'cep' => $faker->postcode,
        'remember_token' => str_random(10),
    ];
});

$factory->define(crud\Phone::class, function (Faker\Generator $faker) {
   
    return [
        'number' => $faker->areaCode . " " .$faker->cellphone(),

    ];
});