<?php

use Illuminate\Database\Seeder;
use crud\User;
use crud\Phone;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    	Phone::truncate();
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        factory(User::class, 10)->create()
    	->each(function($u){
    		$u->telefones()->saveMany(factory(Phone::class, 2)->make());
    	});
    }
}
